/*
 * This C program shows the output of a linux command
 * at every given interval .
 * This program is similar to watch command .
 * */
#include<iostream>
#include<stdlib.h>
#include<unistd.h>
int main(int argc, char *argv[]){
    if(argc < 3){
        printf("Use the command as shown below .");
        printf("Usage: ./watch \"ls -l\" 2\n");
        printf("Above command shows content of current directory every 2 second.\n");
        return 1;
    }

	while(true){
		system("clear");
        printf("Every %s second\n", argv[2]);
		system(argv[1]);
		sleep(atoi(argv[2]));
	}
	return 0;
}
